#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

int 
socket_resolve(const char *host, const char *port, struct addrinfo **hostinfo)
{
     /* Lookup server address information */
    struct addrinfo  hints;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;	/* Use either IPv4 (AF_INET) or IPv6 (AF_INET6) */
    hints.ai_socktype = SOCK_STREAM;	/* Use TCP  */

    return getaddrinfo(host, port, &hints, hostinfo);

}

int 
socket_dial(const char *host, const char *port)
{

    struct addrinfo *hostinfo;
    int socket_fd = -1;

    if(socket_resolve(host, port, &hostinfo) < 0){

	return -1;
    }

    for (struct addrinfo *p = hostinfo; p != NULL; p = p->ai_next) {

	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    continue;
	}

	/* Connect to host */
	if (connect(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    close(socket_fd);
	    continue;
	}

	/* Successful connection */
	break;
    }
    freeaddrinfo(hostinfo);

    return socket_fd;

}

int 
socket_listen(const char *port)
{

    struct addrinfo *hostinfo = NULL;
    int server_fd = -1;

    for(struct addrinfo *p = hostinfo; p != NULL; p = p->ai_next) {
	/* Allocate socket */
	if ((server_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    continue;
	}

	/* Bind socket */
	if (bind(server_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    close(server_fd);
	    continue;
	}

	/* Listen on socket */
	if (listen(server_fd, SOMAXCONN) < 0) {
	    close(server_fd);
	    continue;
	}

	/* Successful setup */
	break;
    }
    freeaddrinfo(hostinfo);

    return server_fd;
} 
