#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include "socket.h"

void
print_help()
{
    puts("usage: ./webcat [-v] URL\n\t-v\tDump HTTP headers with URL contents\n");

}

char*
get_domain(const char* host)
{

    char* double_slash = strstr(host, "//");
    char* single_slash;
    double_slash += 2;

    if((single_slash = strstr(double_slash, "/")) != NULL)
    {
	*single_slash = 0;

    }

    return double_slash;
}

int
main(int argc, char* argv[])
{
    int opt;
    bool take_a_dump = true;
    int sfd;
    const char* port = "80";
    FILE* file;
    char sBUF[BUFSIZ];
    bool start_printing = false;

    while((opt = getopt(argc, argv, "vh")) != -1)
    {
	switch(opt)
	{
	    case 'v':
		take_a_dump = true;
		break;
	    case 'h':
		print_help();
		return 0;
	}
    }

    const char* host = argv[optind];


    char* domain = get_domain(host);
    //char* extension = domain + strlen(domain)+1;
    char* extension = "";
    printf("domain: %s\n", domain);
    printf("ext: %s\n", extension);


    sfd = socket_dial(domain, port); 
    if(sfd == -1)
    {
	return EXIT_FAILURE;
    }

    file = fdopen(sfd, "r+");
    if(file == NULL)
    {
	return EXIT_FAILURE;
    }

    //write to the socket
    fprintf(file, "GET /%s HTTP/1.0\r\n", extension);
    fprintf(file, "Host: %s\r\n", domain);
    fprintf(file, "\r\n");

    //read from the socket
    if(take_a_dump)
    {
	while(fgets(sBUF, BUFSIZ, file) != NULL)
	{
	    printf("%s", sBUF);
	}
    } else {
	while(fgets(sBUF, BUFSIZ, file) != NULL)
	{
	    if(strcmp(sBUF, "\n"))
	    {
		start_printing = true;
	    }
	    if(start_printing)
	    {
		printf("%s", sBUF);
	    }
	}
    }

    return 0;
}
