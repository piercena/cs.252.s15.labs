#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "socket.h"
#include <netinet/in.h>
#include <string.h>
#include <stdbool.h>

bool
has_our_call(char* buffer)
{
	if(strstr(buffer, "!bestsuperhero") != NULL)
	{
		return true;
	}
	return false;
}


int main(int argc, char *argv[])
{
	char *user = "aquab0t 0 * :Aqua Man";
	char *nick = "aquab0t";
	char *chatroom = "uwec-cs";
	char *message = "Aquaman 4lyfe!";
	char *domain = "irc.freenode.net";
	char *port = "6667";
	int sfd = -1;
	FILE *file;
	char buffer[BUFSIZ];

	sfd = socket_dial(domain, port);
	if(sfd == -1)
	{
		return EXIT_FAILURE;
	}

	file = fdopen(sfd, "r+");
	if(file == NULL)
	{
		return EXIT_FAILURE;
	}

	while(fgets(buffer,BUFSIZ,file) != NULL)
	{
		fputs(buffer, stdout);
		if(strstr(buffer, ":*** Found your hostname") != NULL)
		{
			break;
		}
	}
	

	fprintf(file, "USER %s\r\n", user);
	fprintf(file, "NICK %s\r\n",nick);
	while(fgets(buffer,BUFSIZ,file) != NULL)
	{
		fputs(buffer, stdout);
		if(strstr(buffer, ":+i") != NULL)
		{
			break;
		}
	}
	
	fprintf(file, "JOIN #%s\r\n", chatroom);
	
	while(fgets(buffer,BUFSIZ,file) != NULL)
	{
		if(strstr(buffer, "PRIVMSG") != NULL)
		{
			printf("%s\n", buffer);
			if(has_our_call(buffer))
			{
				fprintf(file, "PRIVMSG #%s :%s\r\n", chatroom, message);
			}
		}
	}

puts("EXITING IRC");
return EXIT_SUCCESS;
}
