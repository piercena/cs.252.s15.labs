#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdbool.h>

void
check_for_needle_only(char needle[])
{

	char str[BUFSIZ];
	
	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
		
		char *ret;

	        ret = strstr(str, needle);

        	if(ret != NULL){
                	printf("%s", str);
        	}
	}
        return;
}

void 
find_lines_not_matching(char needle[])
{
	
	char str[BUFSIZ];

        while(fgets(str, BUFSIZ, stdin) != NULL)
        {

        	char *ret;

        	ret = strstr(str, needle);

        	if(ret == NULL){
        	        printf("%s", str);
        	}
	}
        return;	
}

void
find_lines_and_line_number(char needle[])
{
	char str[BUFSIZ];
	int line = 1;

	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
		char *ret;

		ret = strstr(str, needle);
		
		if(ret != NULL){
                        printf("%d:%s", line, str);
                }
		line++;

	}
	return;
}


void
pattern_match_without_cases(char needle[])
{

	char str[BUFSIZ];

        while(fgets(str, BUFSIZ, stdin) != NULL)
        {
		char *ret;

		ret = strcasestr(str, needle);

		if(ret != NULL){
			printf("%s", str);
		}
	}	
}


void
print_help_message()
{
	printf("\t-i Ignore case\n\t-n Include line number\n\t-v Invert match\n\t-h Print help message\n");
	return;
}

int main(int argc, char *argv[])
{

	int i, opt;
	bool hasV, hasN, hasI;

	while ((opt = getopt(argc, argv, "invh")) != -1) {        
	
	switch(opt) {
        case 'v':
		hasV = true;
                break;
        case 'n':
		hasN = true;
                break;
        case 'i':
		hasI = true;
		break;
	case 'h':
		print_help_message();
		return 0;
        }

	}
	

	char *needle = argv[optind];
	
		if(hasV == false && hasN == false && hasI == false){
                	check_for_needle_only(needle);
        	} else {
			if(hasI == true){
				pattern_match_without_cases(needle);
			}
 
			if (hasV == true){
				find_lines_not_matching(needle);
			}

			if (hasN == true){
				find_lines_and_line_number(needle);
			}
		}
	return 0;
}
	
