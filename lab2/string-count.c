#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <ctype.h>

void
count_letters_in_string()
{
	char str[BUFSIZ];
	int length;
	int total = 0;

	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
	length = strlen(str);
	total = total + length;
	}
	printf("%d\n", total);

	return;
}

void
count_words_in_string()
{
	char str[BUFSIZ];
	int length = 0;
	int count = 0;
	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
		length = strlen(str);
		for(int i = 0; i < length; i++)
		{
			
			if(i > 0 && isspace(str[i]) && !isspace(str[i - 1]))
			{
				count++;
			}
			
		}
	}
	printf("%d\n", count);
	return;
}

void
print_newline_count()
{
	char str[BUFSIZ];
        int length = 0;
        int count = 0;

	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
		length = strlen(str);
		for(int i = 0; i < length; i++)
		{
			if(str[i] == '\n')
			{
				count++;
			}
		}
	}
	printf("%d\n", count);
	return;
}

void 
print_help_message()
{
	puts("\t-c Count character\n\t-w Count words\n\t-l Count lines\n\t-h Print help message");
	return;
}

int 
main(int argc, char *argv[])
{

	char str[BUFSIZ];
	int i, opt;
		
	while ((opt = getopt(argc, argv, "cwlh")) != -1) {

	switch(opt) {
	case 'c':
		count_letters_in_string();
		break;
	case 'w':
		count_words_in_string();
		break;
	case 'l':
		print_newline_count();
		break;
	case 'h':
		print_help_message();
		return 0;
	}
	}
	return 0;
}
