#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <ctype.h>

void make_small();
void make_big();
void print_help();

int main(int argc, char *argv[])
{
  char str[BUFSIZ];
  int i, opt;

  while ((opt = getopt(argc, argv, "luh")) != -1) {

	switch(opt) {
	case 'l':
		make_small();
		break;
	case 'u':
		make_big();
		break;
	case 'h':
		print_help();
		return 0;
	}

  }
	return 0;

}

void make_small(){
	
	char str[BUFSIZ];
	
	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
		for(int k = 0; k < strlen(str); k++){
			str[k] = tolower(str[k]);
		}
		printf("%s", str);	
	}
	return;
}

void make_big(){

	char str[BUFSIZ];
	
	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
		for(int i = 0; i < strlen(str); i++){
			str[i] = toupper(str[i]);
		}
		printf("%s", str);
	}
	return;
}

void print_help(){
	printf("usage: ./stringcase [-luh]\n\t-l Convert to lower case\n\t-u Convert to upper case\n\t-h Print help message\n");
}
