#! /bin/bash
if [[ "$#" -ne 1 ]]; then
	du -h $2 2> /dev/null | sort -rh |head --lines=$1;
else
	du -h $1 2> /dev/null | sort -rh | head --lines=10;
fi
