#!/bin/bash

if [ $1 = "sum" ]; then
	awk '{ sum += $0 } END { print sum }'    
elif [ $1 = "mean" ]; then
	awk '{ sum += $0; n++ } END { if (n > 0) print sum / n; }' 
elif [ $1 = "min" ]; then
	awk 'BEGIN{max=0;min=512} { if (max < $1){ max = $1 }; if(min > $1){ min = $1 } } END{ print min}'
elif [ $1 = "max" ]; then
	awk 'BEGIN{max=0;min=512} { if (max < $1){ max = $1 }; if(min > $1){ min = $1 } } END{ print max}'
else 
	echo error: invalid argument passed
fi
