#!/bin/bash

kill -9 $(ps -a | grep $1 | head -n 1 | awk '{print $1}')
