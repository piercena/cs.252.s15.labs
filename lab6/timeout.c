#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>

pid_t pid;

void
print_help_message()
{
    puts("usage: timeout -t timeout command\n\t-t seconds\tHow many seconds to wait");

}

void 
signal_handler(int signo)
{
    int status;
    //kill and wait for the child process
    kill(pid, SIGTERM);
    if(wait(&status) != -1)
    {
	return;
    }
    
}

int 
main(int argc, char* argv[])
{
    int opt, status, time = 10;

    while((opt = getopt(argc, argv, "t:h")) != -1)
    {
	switch(opt)
	{
	    case 'h':
		print_help_message();
		return 0;
	    case 't':
		time = atoi(optarg);
		break;
	}
    }


    //fork
    pid = fork();
    switch(pid)
    {   
	//error
	case -1:
	    perror("fork");
	    exit(EXIT_FAILURE);
	    break; 
	//child 
	case 0:	
	    //child should exec
	    execvp(argv[optind], &argv[optind]);
    	//parent
	default:
	    //set a signal waiting for an alarm to go off
	    signal(SIGALRM, signal_handler);
	    //set alarm
	    alarm(time);
	    //if alarm reaches 0, kill the child
	    if(wait(&status) != -1)
	    {
		return EXIT_SUCCESS;
 	    } else {
		printf("TIME LIMIT OF %d EXCEEDED\n", time);
	    }
    }

    return 0;
}
