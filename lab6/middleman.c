#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

bool exists(const char* fname)
{
    struct stat stat_struct;

    if (stat(fname, &stat_struct) != -1)
    {
        return true;
    }
    return false;
}

void
print_help_statement()
{
    puts("usage: middleman [-E] -c command -f fifo_path\n\t-E\t\tEcho FIFO to STDOUT\n\t-c command\tCommand to write to\n\t-f fifo_path\tPath of FIFO to read");
}

int
main(int argc, char* argv[])
{
    bool echo = false;
    char* command;
    char* fifo_path;
    int opt;
    FILE* file = NULL;
    char str[BUFSIZ] = "";    
    FILE* temp = NULL;

    while((opt = getopt(argc, argv, "Ec:f:h")) != -1)
    {
        switch(opt)
	{
	    case 'E':
	        echo = true;
	 	break;
	    case 'h':
		print_help_statement();
		return 0;
	    case 'c':
		command = optarg;
		break;
	    case 'f':
		fifo_path = optarg;
		break;

	}

    }

    //if fifo_path doesn't exist, make it
    if(!exists(fifo_path))
    {
	if(mkfifo(fifo_path, S_IRUSR|S_IWUSR) != 0)
    	{
	    printf("Error: %s\n", strerror(errno));
	    return EXIT_FAILURE;
	
	}
    }

    //popen command
    if((file = popen(command, "w")) == NULL)
    {
	printf("error opening pipe on: %s\n", command);
	printf("Error: %s\n", strerror(errno));
	return EXIT_FAILURE;
    }
    
    //while popen is not EOF
    while(!feof(file))
    {
        //if opening fifo_path doesn't work, exit
        if((temp = fopen(fifo_path, "r")) == NULL)
    	{	
	    printf("Error: %s\n", strerror(errno));
	    return EXIT_FAILURE;
    	}

	//read and write and echo if necessary
	if(fgets(str, BUFSIZ, temp) != NULL){
	    fputs(str, file);
	    fflush(file);
	    if(echo){
	        printf("%s\n", str);
	    }

	}
	if(temp != NULL){
            //close command
            fclose(temp);
	}
    }
    return 0;
}
