#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

int
main(int argc, char* argv[])
{
    setenv("EDITOR", "vim", 1);
    char* editor = getenv("EDITOR");
    char* temp = "./temp.txt";
    FILE* fp = NULL;
    char str[BUFSIZ];

    //open a temporary file
    if((fp = fopen(temp, "w+")) == NULL)
    {
	puts("FILE could not be created, exiting program");
	return EXIT_FAILURE;
    }

    for(int i = 1; i < argc; i++)
    {
	fprintf(fp,"%s\n", argv[i]);
    }

    fclose(fp);

    snprintf(str, BUFSIZ,"%s %s", editor, temp);

    system(str);
     
    if((fp = fopen(temp, "r")) == NULL)
    {
	puts("FILE could not be created, exiting program");
	return EXIT_FAILURE;
    }
    
    int k = 1;
    
    while(fgets(str, BUFSIZ, fp)){
	if(strcmp(argv[k], str) != 0){
	    str[strlen(str) - 1] = 0;
	    printf("%s\n", str);
	    rename(argv[k], str);
	    k++;
	}
   }

   fclose(fp);
    
   return 0;
}
