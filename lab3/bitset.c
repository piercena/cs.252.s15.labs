#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define NSETS   1024

void
bitset_add(uint64_t sets[], uint64_t set, uint64_t value)
{
    sets[set] = sets[set] | (1UL<<value);
}

bool
bitset_get(uint64_t sets[], uint64_t set, uint64_t value)
{
    return sets[set] & (1UL<<value);
}

void
bitset_del(uint64_t sets[], uint64_t set, uint64_t value)
{
    sets[set] = sets[set] &= ~(1UL<<value);
}

void
bitset_list(uint64_t sets[], uint64_t set)
{
   for(int i = 0; i < 64; i++)
   {
	if(sets[set] & (1UL<<i))
	{
		printf("%d\n", i);
	}
   }

}

void
bitset_bin(uint64_t sets[], uint64_t set)
{
   for(int i = 63; i >= 0; i--){
   	if(sets[set] & (1UL<<i))
   	{
		printf("1");
   	} else {
		printf("0");
   	}
   }
   puts("\n");

}

void
bitset_hex(uint64_t sets[], uint64_t set)
{
	printf("%x\n", sets[set]);
}

void
bitset_dec(uint64_t sets[], uint64_t set)
{
	printf("%d\n", sets[set]);
}

void
bitset_count(uint64_t sets[], uint64_t set)
{
	int count = 0;

	for(int i = 0; i < 64; i++)
	{
		if(sets[set] &(1UL<<i))
		{
			count++;
		}
	}
	printf("%d\n", count);
}

void
bitset_max(uint64_t sets[], uint64_t set)
{

	for(int i = 63; i >= 0; i--)
	{
		if(sets[set] & (1UL<<i))
		{
			printf("%d\n", i);
			break;
		}
	}


}

void
bitset_min(uint64_t sets[], uint64_t set)
{
	for(int i = 0; i < 64; i++)
	{
		if(sets[set] & (1UL<<i))
		{
			printf("%d\n", i);
			break;
		}
	}
}

void
bitset_union(uint64_t sets[], uint64_t set1, uint64_t set2)
{
	for(int i = 0; i < 64; i++)
	{
		if(sets[set1] & (1UL<<i))
		{
			printf("%d\n"), i;
		} else if (sets[set2] & (1UL<<i)) {
			printf("%d\n", i);
		}
	}
}

void
bitset_intersection(uint64_t sets[], uint64_t set1, uint64_t set2)
{
	uint64_t temp[1] = {0};
	temp[0] = sets[set1] & sets[set2];

	bitset_list(temp, 0);
}

void
bitset_difference(uint64_t sets[], uint64_t set1, uint64_t set2)
{
	uint64_t temp[1] = {0};
	temp[0] = sets[set1] ^ sets[set2];

	bitset_list(temp, 0);
}

void
bitset_distance(uint64_t sets[], uint64_t set1, uint64_t set2)
{
	uint64_t temp[1] = {0};
	temp[0] = sets[set1] ^ sets[set2];

	bitset_count(temp, 0);
}

int
main(int argc, char *argv[])
{
    char     buffer[BUFSIZ];
    uint64_t command = 0;
    uint64_t sets[NSETS] = {0};

    while (true) {
        uint64_t set;
	uint64_t set2;
        uint64_t value;

        printf("[%4lu> ", command);
        fflush(stdout);

        if (fgets(buffer, BUFSIZ, stdin) == NULL) {
            break;
        }

        if (sscanf(buffer, "ADD %lu %lu ", &set, &value) == 2) {
            bitset_add(sets, set, value);
        } else if (sscanf(buffer, "GET %lu %lu", &set, &value) == 2) {
            printf("GET %lu %lu = %s\n", set, value, bitset_get(sets, set, value) ? "Yes" : "No");
        } else if (sscanf(buffer, "DEL %lu %lu", &set, &value) == 2) {
	    bitset_del(sets, set, value);
	} else if (sscanf(buffer, "LIST %lu", &set) == 1) {
	    bitset_list(sets, set);
	} else if (sscanf(buffer, "BIN %lu", &set) == 1) {
	    bitset_bin(sets, set);
	} else if (sscanf(buffer, "HEX %lu", &set) == 1) {
	    bitset_hex(sets, set);
	} else if (sscanf(buffer, "DEC %lu", &set) == 1) {
	    bitset_dec(sets, set);
	} else if (sscanf(buffer, "COUNT %lu", &set) == 1) {
	    bitset_count(sets, set);
	} else if (sscanf(buffer, "MAX %lu", &set) == 1) {
	    bitset_max(sets, set);
	} else if (sscanf(buffer, "MIN %lu", &set) == 1) {
	    bitset_min(sets, set);
	} else if (sscanf(buffer, "UNION %lu %lu", &set, &set2) == 2) {
	    bitset_union(sets, set, set2);
	} else if (sscanf(buffer, "INTERSECTION %lu %lu", &set, &set2) == 2) {
	    bitset_intersection(sets, set, set2);
	} else if (sscanf(buffer, "DIFFERENCE %lu %lu", &set, &set2) == 2) {
	    bitset_difference(sets, set, set2);
	} else if (sscanf(buffer, "DISTANCE %lu %lu", &set, &set2) == 2) {
	    bitset_distance(sets, set, set2);
	} else {
            printf("Unknown command: %s", buffer);
        }

        command++;
    }

    return EXIT_SUCCESS;
}
