#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>

using namespace std;

typedef set<string> Edges;
typedef map<string, Edges> Graph;

static void
read_graph(Graph &g)
{
    string line;

    while (getline(cin, line)) {
	stringstream ss(line);
	string	     node;
	string	     edge;

	ss >> node;
	g[node] = set<string>();

	while (ss >> edge) {
	    g[node].insert(edge);
	}
    }
}

static void
print_graph(Graph &g)
{
    for (auto node : g) {
	cout << node.first;
	for (auto edge : node.second) {
	    cout << " " << edge;
	}
	cout << endl;
    }
}


int
main(int argc, char *argv[])
{
    Graph g;

    read_graph(g);
    print_graph(g);

    return EXIT_SUCCESS;
}
