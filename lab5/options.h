#ifndef __OPTIONS_H__
#define __OPTIONS_H__
#include <stdlib.h>

struct Options {
   FILE *if_;
   FILE *of;
   size_t seek;
   size_t skip;
   size_t bs;
   size_t count;
};

#endif
