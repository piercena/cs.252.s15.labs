#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "options.h"
#include <limits.h>

static const char *DELIMITING_CHAR = "=";

void
print_struct(struct Options *Options_list)
{
printf("%p\n", Options_list->if_);
printf("%p\n", Options_list->of);
printf("%zu\n", Options_list->bs);
printf("%zu\n", Options_list->count);
printf("%zu\n", Options_list->seek);
printf("%zu\n", Options_list->skip);
return;
}

void
get_options_from_standard_in(int argc, char *argv[],struct Options *Options_list)
{
   char *token;

   for(int i = 1; i < argc; i++)
   {
	token = strtok(argv[i], DELIMITING_CHAR);

	if(strcmp(token, "if") == 0)
	{
	   token = strtok(NULL, DELIMITING_CHAR);
	   FILE *fp = fopen(token, "r");
	   Options_list->if_ = fp;

	} else if(strcmp(token, "of") == 0) {

	   token = strtok(NULL, DELIMITING_CHAR);
	   FILE *fp = fopen(token, "w+");
	   Options_list->of = fp;

	} else if(strcmp(token, "count") == 0) {

	   token = strtok(NULL, DELIMITING_CHAR);
	   Options_list->count = atoi(token);

	} else if(strcmp(token, "bs") == 0) {

	   token = strtok(NULL, DELIMITING_CHAR);
	   Options_list->bs = atoi(token);

	} else if(strcmp(token, "seek") == 0) {

	   token = strtok(NULL, DELIMITING_CHAR);
	   Options_list->seek = atoi(token);

	} else if(strcmp(token, "skip") == 0) {

	   token = strtok(NULL, DELIMITING_CHAR);
	   Options_list->skip = atoi(token);
	}
   }


   return;
}

void
set_null_values(struct Options *Options_list)
{

   if(Options_list->if_ == NULL)
   {
	Options_list->if_ = stdin;
   }
   if(Options_list->of == NULL)
   {
	Options_list->of = stdout;
   }
   if(Options_list->bs == 0)
   {
	Options_list->bs = BUFSIZ;
   }
   if(Options_list->count == 0)
   {
	Options_list->count = ULONG_MAX;
   }

   return;
}

void
process_result(struct Options *Options_list)
{
   if(Options_list->count == 0)
   {
	return;
   }

   size_t bs = Options_list->bs;
   void *str[bs];
   size_t nread;
   int num_of_read_writes = 0;

   fseek(Options_list->if_, Options_list->skip, SEEK_SET);
   fseek(Options_list->of, Options_list->seek, SEEK_SET);

   while((nread = fread(str, sizeof(char), bs, Options_list->if_)) > 0)
   {
	fwrite(str, sizeof(char), nread, Options_list->of);

	num_of_read_writes++;

	if(num_of_read_writes >= Options_list->count)
	{
	   break;
	}
   }
   return;
}

int
main(int argc, char *argv[])
{

   struct Options *Options_list = malloc(sizeof(struct Options));

   get_options_from_standard_in(argc, argv, Options_list);

   set_null_values(Options_list);

   process_result(Options_list);

   return 0;
}


