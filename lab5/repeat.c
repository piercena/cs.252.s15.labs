#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>

char str[BUFSIZ];
void
print_help_statement()
{
   printf("usage: ./repeat [-ah] FILE ...\n\t-a\tAppend to the given FILEs, do not overwrite\n\t-h\tDisplay this help and exit\n");

   return;
}

void
append(FILE *files[], int file_count)
{
	//declare string to hold contents of fgets buffer
	char str[BUFSIZ];

	//while the string is not null
	while(fgets(str, BUFSIZ, stdin) != NULL)
	{
	//iterate through each file and print
	//the contents of buffer to the file
	for(int i = 0; i < file_count; i++)
	{
	   FILE *f = files[i];

	   if(f != NULL)
	   {
		   fprintf(f, "%s", str);

   	   }
	}
	}
   return;
}

int
main(int argc, char *argv[])
{
   int num_of_files = 1;
   int opt;
   FILE *files[argc];
   files[0] = stdout;
   bool append_boolean = false;

   //check for flags
   while((opt = getopt(argc, argv, "a:h")) != -1) {

	switch(opt) {
	case 'a':
		append_boolean = true;
		break;
	case 'h':
		print_help_statement();
		return 0;
	}

   }


   for(int i = optind - 1; i < argc; i++)
   {
	//if dash is found add stdin to the file pointer array
	if(strcmp(argv[i], "-") == 0){

	   files[num_of_files] = stdout;
	} else {
	   //if appending, open file in append mode
	   //else open in write mode
	   if(append_boolean)
	   {
	   	FILE *f = fopen(argv[i], "a+");
		files[num_of_files] = f;
	   } else  {
		FILE *f = fopen(argv[i], "w");
	   	files[num_of_files] = f;
	   }
	}
	num_of_files++;
   }

   append(files, argc);

   return 0;

}
