#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <limits.h>

char
determine_path_type(const char *path)
{
   struct stat st;
   char type = '?';

   if(stat(path, &st) != 0)
   {
	printf("Unable to stat %s: %s\n", path, strerror(errno));
	goto exit;
   }

   switch(st.st_mode & S_IFMT)
   {
	case S_IFBLK:  type = 'B'; break;
	case S_IFCHR:  type = 'C'; break;
	case S_IFDIR:  type = 'D'; break;
	case S_IFIFO:  type = 'P'; break;
	case S_IFLNK:  type = 'L'; break;
	case S_IFREG:  type = 'F'; break;
	case S_IFSOCK: type = 'S'; break;
	default:       type = '?'; break;
   }

exit:
    return(type);
}

int
determine_stream_size(const char *path)
{
   struct stat st;
   int stream_size = 0;

   if(stat(path, &st) != 0)
   {
	printf("Unable to stat %s: %s\n", path, strerror(errno));
	goto exit;
   }

   stream_size = st.st_size;

exit:
    return(stream_size);
}


int
print_usage_for_all(const char *path, const bool print_all)
{
   DIR* dir;
   struct dirent *entry;
   char type;
   char fullpath[PATH_MAX];
   int stream_size = 0;
   int total_size = 0;
   int dir_size = 0;

   dir = opendir(path);
   if(dir == NULL)
   {
	printf("Unable to opendir %s: %s\n", path, strerror(errno));
   }


   while ((entry = readdir(dir)) != NULL)
   {
	if (strcmp(entry->d_name, ".") == 0)
	{
	   total_size += determine_stream_size(path);
	   continue;
	}

	if(strcmp(entry->d_name, "..") == 0)
	{
	   continue;
	}

	snprintf(fullpath, PATH_MAX, "%s/%s", path, entry->d_name);
	type = determine_path_type(fullpath);
	stream_size = determine_stream_size(fullpath);
	total_size += stream_size;

	if(print_all)
	{
	   printf("%d\t%s\n", stream_size, fullpath);
	}

	if(type == 'D')
	{
	   dir_size = print_usage_for_all(fullpath, print_all);
	   total_size += dir_size;

	   if(print_all)
	   {
	      printf("%d\t%s\n", dir_size, fullpath);
	   }
	}
   }

   return total_size;

}

int
main(int argc, char *argv[])
{

   int opt;
   bool all_dirs = false;
   bool summ_dirs = false;
   int dir_size = 0;
   bool print_all = false;

   while((opt = getopt(argc, argv, "as")) != -1)
   {
	switch(opt)
	{
	   case 'a':
		all_dirs = true;
		break;
	   case 's':
		summ_dirs = true;
		break;
	}
   }

   if(all_dirs)
   {
	print_all = true;

	for(int i = optind; i < argc; i++){
	 	dir_size = print_usage_for_all(argv[i], print_all);
		printf("%d\t%s\n", dir_size, argv[i]);
	}
   }

   if(summ_dirs)
   {
	for(int i = optind; i < argc; i++)
	{
	   dir_size = print_usage_for_all(argv[i], print_all);
	   printf("%d\t%s\n", dir_size, argv[i]);
	}
   }

   return 0;
}
