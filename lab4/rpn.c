#include "list.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

void
process_line(char buffer[])
{


	void* token = strtok(buffer, " \t\n");
	struct list_t *list;
	list = list_create();
	int64_t operand1, operand2, result;

	while (token)
	{

		if(strcmp(token, "+") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand1 + operand2;
			list_push_front(list, (void*)result);
	   	}
		else if(strcmp(token, "*") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand1 * operand2;
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, "-") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 - operand1;
			list_push_front(list, (void*)result);
		}
	   	else if(strcmp(token, "/") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 / operand1;
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, "^") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 ^ operand1;
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, "%") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 % operand1;
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, "~") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			result =  ~(operand1);
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, "|") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 | operand1;
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, "&") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 & operand1;
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, "<<") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 << operand1;
			list_push_front(list, (void*)result);
		}
		else if(strcmp(token, ">>") == 0)
		{
			operand1 = (int64_t)list_pop_front(list);
			operand2 = (int64_t)list_pop_front(list);
			result = operand2 >> operand1;
			list_push_front(list, (void*)result);
		}
		else {
			result = strtol(token, NULL, 10);
			list_push_front(list, (void*) result);
		}

	   token = strtok(NULL, " \t\n");
}

	result = (int64_t)(list_pop_front(list));

	printf("%" PRId64 "\n", result);
	return;
}


int
main(int argc, char *argv[])
{

    char buffer[BUFSIZ];

    while(fgets(buffer, BUFSIZ, stdin) != NULL)
    {
	process_line(buffer);

    }

	return 0;
}
