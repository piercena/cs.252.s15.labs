#include "list.h"

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

static const long   TEST_DATA[] = {5, 4, 7, 0, 1};
static const size_t TEST_SIZE   = sizeof(TEST_DATA)/sizeof(long);

void
test_node_create()
{
    struct node_t *node;

    node = node_create((void*)INT_MAX, NULL, NULL);
    assert(node != NULL);
    assert(node->data == (void*)INT_MAX);
    assert(node->next == NULL);
    assert(node->prev == NULL);

    node_delete(node, false);
}

void
test_list_create()
{
    struct list_t *list;

    list = list_create();
    assert(list != NULL);
    assert(list->size == 0);
    assert(list->head == NULL);
    assert(list->tail == NULL);

    list_delete(list, false);
}

void
test_list_size()
{
    struct list_t *list;

    list = list_create();
    assert(list != NULL);

    for (size_t i = 0; i < TEST_SIZE; i++) {
        list_push_front(list, (void *)TEST_DATA[i]);
    }

    assert(list_size(list) == TEST_SIZE);

    list_delete(list, false);
}

void
test_list_push_front()
{
    struct list_t *list;

    list = list_create();

    for(int i = 0; i < TEST_SIZE; i++){

	list_push_front(list,(void *) TEST_DATA[i]);
	assert(list->size == (i + 1));
    }


}

void
test_list_push_back()
{
	struct list_t *list;

	list = list_create();

	for(int i = 0; i < TEST_SIZE; i++)
	{
		list_push_back(list,(void *) TEST_DATA[i]);
		assert(list->size == (i + 1));
	}
}

void
test_list_data()
{
	struct list_t *list;

	list = list_create();

	for(int i = 0; i < TEST_SIZE; i++)
	{
		list_push_front(list,(void *) TEST_DATA[i]);
	}

	for(int i = 0; i < TEST_SIZE; i++)
	{
		assert(list_find(list,(void *) TEST_DATA[i]) != NULL);
	}
}

void
test_list_pop_front()
{
	struct list_t *list;

	list = list_create();
	for(int i = 0; i < TEST_SIZE; i++)
	{
		list_push_front(list,(void *) TEST_DATA[i]);
	}

	for(int i = 4; i >= 0; i--)
	{
		assert(list_pop_front(list) == (void*) TEST_DATA[i]);
	}
}

void
test_list_pop_back()
{
	struct list_t *list;

	list = list_create();
	for(int i = 0; i < TEST_SIZE; i++)
	{
		list_push_back(list,(void *) TEST_DATA[i]);
	}

	for(int i = 4; i >= 0; i--)
	{

		assert(list_pop_back(list) == (void *) TEST_DATA[i]);
	}
}

void
test_list_find()
{
	struct list_t *list;

	list = list_create();
	for(int i = 0; i < TEST_SIZE; i++)
	{
		list_push_front(list, (void *) TEST_DATA[i]);
	}

	assert(list_find(list,(void *) TEST_DATA[0]) != NULL);
}

void
test_list_remove()
{
	struct list_t *list;

	list = list_create();
	for(int i = 0; i < TEST_SIZE; i++)
	{
		list_push_front(list, (void *) TEST_DATA[i]);
	}

	for(int i = 0; i < TEST_SIZE; i++)
	{
		assert(list->size == TEST_SIZE - i);
		struct node_t *temp = node_create((void*) TEST_DATA[i], NULL, NULL);
		list_remove(list, temp);
		assert(list->size == TEST_SIZE - i - 1);
	}

}

int
main(int argc, char *argv[])
{
    test_node_create();
    test_list_create();
    test_list_size();
    test_list_push_front();
    test_list_push_back();
    test_list_data();
    test_list_pop_front();
    test_list_pop_back();
    test_list_find();
    test_list_remove();

    return EXIT_SUCCESS;
}
