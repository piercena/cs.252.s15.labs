#ifndef __LIST_H__
#define   __LIST_H__
#include <stdlib.h>
#include <stdbool.h>

//declarations
struct node_t {
  void		*data; /* contains nodes data */
  struct node_t *next; /* link to next node */
  struct node_t *prev; /* link to previous node */
};

struct list_t {
  size_t 	size; /* size of list */
  struct node_t *head; /* link to first node */
  struct node_t *tail; /* link to last node */
};

struct node_t *node_create(void *data, struct node_t *next, struct node_t *prev);
void node_delete(struct node_t *node, bool free_data);
struct list_t *list_create();
void list_delete(struct list_t *list, bool free_data);
size_t list_size(struct list_t *list);
void list_push_front(struct list_t *list, void *data);
void list_push_back(struct list_t *list, void *data);
void *list_pop_front(struct list_t *list);
void *list_pop_back(struct list_t *list);
struct node_t *list_find(struct list_t *list, void *data);
void list_remove(struct list_t *list, struct node_t *node);


#endif
