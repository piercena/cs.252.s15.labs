#include "list.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>


//constructors and destrutors
struct node_t *node_create(void *data, struct node_t *next, struct node_t *prev)
{
   struct node_t *node = malloc(sizeof(struct node_t));

   if(node != NULL){
	node->data = data;
	node->next = next;
	node->prev = prev;
   }

   return node;
}

void node_delete(struct node_t *node, bool free_data)
{
   if(node != NULL) {
   	if(free_data)
   	{
		free(node->data);
   	}
	free(node);
   }
   return;
}

struct list_t *list_create()
{
   struct list_t *list = calloc(1, sizeof(struct list_t));

   return list;
}

void list_delete(struct list_t *list, bool free_data)
{
   if(list != NULL)
   {
	/*for each node in the list*/
	struct node_t *current = list->head;

	while(current != NULL)
	{
		struct node_t *temp = current->next;
		node_delete(current, free_data);
		current = temp;
	}
	free(list);
   }

   return;
}

//operations
size_t list_size(struct list_t *list)
{
	return list->size;
}

void list_push_front(struct list_t *list, void *data)
{
	struct node_t *temp = node_create(data, list->head, NULL);

	if(list->size == 0){
		list->head = temp;
		list->tail = temp;
	} else {
		list->head->prev = temp;
		list->head = temp;
	}

	list->size++;
	return;
}

void *list_pop_front(struct list_t *list)
{
	if(list->size != 0)
	{
		struct node_t *temp = list->head;
		void *data = list->head->data;
		list->head = list->head->next;
		if(list->head == NULL)
		{
			list->tail = NULL;
		}
		list->size--;
		node_delete(temp, false);
		return data;
	}
	return NULL;
}

void list_push_back(struct list_t *list, void *data)
{
	struct node_t *temp = node_create(data, NULL, list->tail);

	if(list->size == 0){
		list->head = temp;
		list->tail = temp;
	} else {
		list->tail->next = temp;
		list->tail = temp;
	}

	list->size++;
	return;
}

void *list_pop_back(struct list_t *list)
{

	if(list->size != 0)
	{
		struct node_t *temp = list->tail;
		void *data = list->tail->data;
		list->tail = list->tail->prev;
		if(list->tail == NULL)
		{
			list->head = NULL;
		}
		list->size--;
		node_delete(temp, false);
		return data;
	}

	return NULL;
}

struct node_t *list_find(struct list_t *list, void *data)
{
   if(list != NULL)
   {
	struct node_t *temp_node = list->head;
	while(temp_node != NULL)
	{
		if(temp_node->data == data)
		{
			return temp_node;
		}

	   temp_node = temp_node->next;
	}
   }
	return NULL;
}

void list_remove(struct list_t *list, struct node_t *node)
{

   if(list != NULL)
   {

	if(list->head->data == node->data)
	{
	   struct node_t *temp = list->head;
	   list->head = list->head->next;
	   node_delete(temp, false);
	   list->size--;
	   return;
	} else {

	   struct node_t *temp = list->head->next;
	   while(temp != NULL)
	   {
		if(temp->data == node->data)
		{
		   temp->prev->next = temp->next;
		   if(temp->next != NULL)
		   {
			temp->next->prev = temp->prev;
		   }
		   node_delete(temp, false);
		   list->size--;
		   return;
		}

		temp = temp->next;
	   }
	}

   }

   return;
}
